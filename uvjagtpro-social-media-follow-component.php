<?php

	/**
	* Plugin Name: UVjagtPro - Facebook like banner
	* Description: This plugin a banner for visitors to like the UVjagtPro Facebook page.
	* Author: Kim Nyegaard Andreasen
	* Version: 1.0
	*/


	function facebook_js() 
	{
	 
		?>
			<!-- Load Facebook SDK for JavaScript -->

			<div id="fb-root"></div>
			
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = 'https://connect.facebook.net/da_DK/sdk.js#xfbml=1&version=v3.1&appId=145201602494887&autoLogAppEvents=1';
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
		<?php
	} 

	add_action( 'storefront_before_site', 'facebook_js' );


	function facebook_banner() 
	{
		?>
			
			<div class="facebook-like-banner">
				<p>Følg UVjagtPro på Facebook</p> 
				<!-- Your like button code -->		
				<div class="fb-like" data-href="https://www.facebook.com/uvjagtpro/" data-width="200" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
			</div>
		<?php
	} 

	add_action( 'storefront_before_footer', 'facebook_banner' );

?>